#!/usr/bin/python3
"""Обробка набору даних літнього одягу"""


import os
import sys
import csv
import time


NECESSARY_KEYS = ['price', 'rating', 'rating_count', 'rating_five_count',
                  'origin_country', 'product_id']
UNKNOWN_COUTNRY = 'UNKNOWN_COUTNRY'


def read_csv(path_to_csv):
    """
    Method to read csv file and obtain the data from it.
    Метод для читання csv файла і отримання даних з нього.

    Parameters:
        path_to_dataset -(str) шлях до csv файла
                               path to .csv file

    Returns:
        (list) список діктів, кожен відповідає одному рядку
               list of dictionaries, each representing one row
    """

    with open(path_to_csv) as csvfile:
        reader = csv.reader(csvfile)
        result_list = []
        headers = next(reader)
        for row in reader:
            record = {}
            for i, value in enumerate(row):
                record[headers[i]] = value
            result_list.append(record)
        return result_list


def clean_up_data(whole_data):
    """ Method to clean up data.

    Parameters:
        whole_data - list of flat dicts

    Returns:
        list of flat dicts, each with less number of keys.
      """
    cleaned_data = [{key: record[key] for key in NECESSARY_KEYS}
                    for record in whole_data]
    return cleaned_data


def group_items_by_countries(cleaned_data):
    """ Method to group products by countries

    Parameters:
        cleaned_data - list of flat dicts
    Returns:
        dict of nested lists. Keys of dicts are country name literals.
                Each country list contains lists of products (flat lists).
    """
    items_by_countries = {}
    for record in cleaned_data:
        origin_country = record['origin_country'] or UNKNOWN_COUTNRY
        if origin_country not in items_by_countries:
            items_by_countries[origin_country] = []
        items_by_countries[origin_country].append(record)
    return items_by_countries


def analyze_5_star(grouped_products):
    """ Method to prepare and print final result.

    Parameters:
        grouped_by_origin: dict of lists

    """
    print('Average price     fivestar_percentage   country')
    for country_name in sorted(grouped_products.keys()):
        country_products = grouped_products[country_name]
        number_of_products = len(country_products)
        avg_price = round(sum([float(product['price'])
                               for product in country_products])/
                          number_of_products, 2)
        country_rating_count = sum([int(product['rating_count'] or '0')
                                    for product in country_products])
        country_fivestar_count = sum([int(product['rating_five_count'] or '0')
                                      for product in country_products])
        if not country_rating_count == 0:
            country_fivestar_ratio = country_fivestar_count / country_rating_count
            country_fivestar_representation = '{} %'.format(
                round(country_fivestar_ratio*100, 2))
        else:
            country_fivestar_representation = 'N/A    '
        print('{}             {}               {} '
              .format(avg_price, country_fivestar_representation,
                      country_name))


if __name__ == '__main__':
    time_start = time.time()
    if len(sys.argv) != 2:
        print('Правильне використання: '
              'python3 solution.py <шлях до summer_products.csv>')
        sys.exit()

    path_to_dataset = sys.argv[1]
    if not os.path.isfile(path_to_dataset):
        print("Файл {} не знайдено".format(path_to_dataset))
        sys.exit()

    print("Файл {} знайдено".format(path_to_dataset))
    data = read_csv(path_to_dataset)
    print("Файл {} прочитано".format(path_to_dataset))
    print()
    data_cleaned = clean_up_data(data)
    grouped_items = group_items_by_countries(data_cleaned)
    analyze_5_star(grouped_items)
    print("Затрачений час: {} секунд.".format(round(time.time() - time_start, 3)))
