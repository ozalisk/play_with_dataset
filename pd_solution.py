#!/usr/bin/python3
"""Обробка набору даних літнього одягу"""

import os
import sys
import csv
import time

import numpy as np
import pandas as pd


UNKNOWN_COUNTRY = 'UNKNOWN_COUNTRY'


def process_data(path_to_dataset):
    """ Process dataset
    Parameters:
        path_to_dataset  (str) path to .csv file containing the dataset.
    Return:
        dataframe
    """
    result = pd.DataFrame(columns=['avg_price', 'fivestar_rating','origin_country'])
    df = pd.read_csv(path_to_dataset)
    df['origin_country'] = df['origin_country'].fillna(UNKNOWN_COUNTRY)
    gr_country = df.groupby('origin_country', dropna=False)
    for origin_country, group in gr_country:
        avg_price = group['price'].mean()
        fivestar_rating = group['rating_five_count'].sum()/(group['rating_count'].sum() or 1)

        result.loc[len(result.index)] = [avg_price, fivestar_rating, origin_country]

    result['avg_price'] = result['avg_price'].apply(lambda x: '{:.2f}'.format(x))
    result['fivestar_rating'] = result['fivestar_rating'].apply(lambda x: '{:.2%}'.format(x))

    return result

if __name__ == '__main__':
    time_start = time.time()
    if len(sys.argv) != 2:
        print('Правильне використання: '
              'python3 solution.py <шлях до summer_products.csv>')
        sys.exit()
    path_to_dataset = sys.argv[1]
    if not os.path.isfile(path_to_dataset):
        print("Файл {} не знайдено".format(path_to_dataset))
        sys.exit()

    result = process_data(path_to_dataset)
    print(result)
    print("Затрачений час: {} секунд.".format(round(time.time() - time_start, 3)))
